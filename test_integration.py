"""
Integration tests for word_finder module.
"""


import unittest
import word_finder as wf


class IntegrationTests(unittest.TestCase):
    """
    This class contains integration tests only.
    """

    def test_encode_soundex(self):
        """
        Passed string should be encoded using American Soundex.
        """

        self.assertEqual(wf.encode_soundex("Robert"), "R163")
        self.assertEqual(wf.encode_soundex("Rupert"), "R163")
        self.assertEqual(wf.encode_soundex("Rubin"), "R150")
        self.assertEqual(wf.encode_soundex("Ashcraft"), "A261")
        self.assertEqual(wf.encode_soundex("Aschcroft"), "A261")
        self.assertEqual(wf.encode_soundex("Tymczak"), "T522")
        self.assertEqual(wf.encode_soundex("Pfister"), "P236")
        self.assertEqual(wf.encode_soundex("Pfipter"), "P136")
        self.assertEqual(wf.encode_soundex("Pfipper"), "P160")
        self.assertEqual(wf.encode_soundex("Pfippip"), "P110")
        self.assertEqual(wf.encode_soundex("Honeyman"), "H555")
        self.assertEqual(wf.encode_soundex("Lithuania"), "L350")
        self.assertEqual(wf.encode_soundex("Lithuanian"), "L355")
        self.assertEqual(wf.encode_soundex("Lietuva"), "L310")
        self.assertEqual(wf.encode_soundex("Listeni"), "L235")
        self.assertEqual(wf.encode_soundex("living"), "l152")


if __name__ == '__main__':
    unittest.main()
