# Word matcher

This program is A CLI tool that matches a phrase (single word) in a given text file. American Soundex algorithm is used to match words.  The search phrase (single word) can be misspelled. Top 5 unique best matched words are returned.

This program is built on Pandas and NumPy to take advantage of [vectorized operations](https://www.oreilly.com/library/view/python-for-data/9781449323592/ch04.html) and can process large text files (>300 MB).

## Setup

Paste the following commands to the Terminal (Mac OS) or [CygWin](https://cygwin.com/install.html) Terminal (Windows). 
Alternatively, if you don't want to use CygWin, launch the same commands with Windows Command Prompt.
```
$ git clone https://gitlab.com/dnorkus/word_matcher.git
$ cd word_matcher
$ pip install -r requirements.txt
```

## Testing

The tests should be run in the following order (from low to the higest level):
```
$ test_unittests.py
$ test_integration.py
$ test_end2end.py
```
## Run

```
$ ./word_finder.py -f a_text_file.txt -w word
```
## Sample usage

Sample file **wiki_lt.txt**:
```
Lithuania (UK and US: Listeni/ˌlɪθuːˈeɪniə/,[11][12][13] Lithuanian: Lietuva
    [lʲɪɛtʊˈvɐ]), officially the Republic of Lithuania (Lithuanian: Lietuvos
    Respublika), is a country in Northern Europe.[14] One of the three Baltic
    states, it is situated along the southeastern shore of the Baltic Sea, to the
    east of Sweden and Denmark. It is bordered by Latvia to the north, Belarus to
    the east and south, Poland to the south, and Kaliningrad Oblast (a Russian
    exclave) to the southwest. Lithuania has an estimated population of 2.9 million
    people as of 2015, and its capital and largest city is Vilnius. Lithuanians are
    a Baltic people. The official language, Lithuanian, along with Latvian, is one
    of only two living languages in the Baltic branch of the Indo-European language
    family.
```

Command line input:
```
$ ./word_finder.py -f wiki_lt.txt -w lituania
```

Produces the following output:
```
Lithuania
Lithuanian
Lithuanians
Lietuva
Listeni
```

## Meta
Donatas Norkus - donatasnorkus@gmail.com

Distributed under MIT license. See ``LICENSE`` for more information.

https://gitlab.com/dnorkus/word_matcher.git