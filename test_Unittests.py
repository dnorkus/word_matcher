"""
Unit tests for word_finder module.
"""


import unittest
import word_finder as wf
import send2trash


class UnitTests(unittest.TestCase):
    """
    This class contains unit tests only.
    """

    @classmethod
    def setUpClass(cls):
        """
        Creates a text file with some information.
        """

        with open('test_file.txt', 'a') as f:
            f.write('First line with info \n'*1024)

    @classmethod
    def tearDownClass(cls):
        """
        Deletes the created file after the test is complete.
        """

        send2trash.send2trash('test_file.txt')

    def test_clean_up_chars(self):
        """
        Passed string should be stripped from punctuation
        and escape sequence characters.
        """

        txt = "it's been a while!\n"
        self.assertEqual(wf.clean_up_chars(txt), "it s been a while  ")

    def test_replace_cons(self):
        """
        All consonants should be replaced with a number.
        """

        self.assertEqual(wf.replace_cons('vpfb'), '1111')
        self.assertEqual(wf.replace_cons('zxsqkjgc'), '22222222')
        self.assertEqual(wf.replace_cons('td'), '33')
        self.assertEqual(wf.replace_cons('l'), '4')
        self.assertEqual(wf.replace_cons('nm'), '55')
        self.assertEqual(wf.replace_cons('r'), '6')

    def test_compare_str_similarity_ratio(self):
        """
        Two identical strings/words (despite uppercase)
        should match with a ratio of 1.0000.
        """

        self.assertEqual(wf.two_str_similarity_ratio('apple', 'Apple'), 1.0000)
        self.assertEqual(wf.two_str_similarity_ratio('apple', 'Aple'), 0.8889)

    def test_get_cl_args(self):
        """
        Command line arguments should be parsed correctly
        """

        cmd_line_input = 'word_finder.py -f a_file.txt -w lituania'
        args = wf.get_cl_args(cmd_line_input.split())
        self.assertEqual(args.filename, 'a_file.txt')
        self.assertEqual(args.word, 'lituania')

    def test_file_chunks(self):
        """
        File should be divided into correct chunks.
        """

        self.assertEqual(wf.file_chunks('test_file.txt'), [(0, 23552)])


if __name__ == '__main__':
    unittest.main()
