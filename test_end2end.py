"""
Integration tests for word_finder module.
"""


import unittest
import word_finder as wf


class End2EndTests(unittest.TestCase):
    """
    This class contains end to end tests only.
    """

    def test_main(self):
        """
        The system should exit if sys args are incorrect.
        """

        with self.assertRaises(SystemExit):
            wf.main(['word_finder.py'])


if __name__ == '__main__':
    unittest.main()
